<?php
/**
 * Shopin functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Shopin
 */

if ( ! function_exists( 'shopin_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function shopin_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Shopin, use a find and replace
	 * to change 'shopin' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'shopin', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'shopin' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'shopin_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'shopin_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function shopin_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'shopin_content_width', 640 );
}
add_action( 'after_setup_theme', 'shopin_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function shopin_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'shopin' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'shopin' ),
		'before_widget' => '<section id="%1$s" class="sky-form widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="cate">',
		'after_title'   => '</h4>',
	) );

	register_sidebar(array(
		'name'			=> esc_html__( 'Footer', 'shopin' ),
		'id'			=> 'footer',
		'description'	=> esc_html__('Add widgets here'),
		'before_widget'	=> '<div class="col-md-3 footer-middle-in">',
		'after_widget'	=>	'</div>',
		'before_title'	=>	'<h6>',
		'after_title'	=> '</h6>',
	));
}
add_action( 'widgets_init', 'shopin_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function shopin_scripts() {
	wp_enqueue_style( 'shopin-style', get_stylesheet_uri() );

	wp_enqueue_script( 'shopin-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'shopin-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'shopin_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Themes Scripts
 */
function theme_scripts() {
	// css files
	wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.css', array(), '1.1', 'all');
	wp_enqueue_style('chocolat', get_template_directory_uri() . '/assets/css/chocolat.css', array(), '1.1', 'all');
	wp_enqueue_style('flexslider', get_template_directory_uri() . '/assets/css/flexslider.css', array(), '1.1', 'all');
	wp_enqueue_style('form', get_template_directory_uri() . '/assets/css/form.css', array(), '1.1', 'all');
	wp_enqueue_style('jstarbox', get_template_directory_uri() . '/assets/css/jstarbox.css', array(), '1.1', 'all');
	wp_enqueue_style('popuo-box', get_template_directory_uri() . '/assets/css/popuo-box.css', array(), '1.1', 'all');
	wp_enqueue_style('style', get_template_directory_uri() . '/assets/css/style.css', array(), '1.1', 'all');
	wp_enqueue_style('style4', get_template_directory_uri() . '/assets/css/style4.css', array(), '1.1', 'all');


	// scripts files
	wp_enqueue_script( 'Jq', get_template_directory_uri() . '/assets/js/jquery.min.js', array (), 1.1, true);
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array ( 'jquery' ), 1.1, true);
	wp_enqueue_script( 'classie', get_template_directory_uri() . '/assets/js/classie.js', array ( 'jquery' ), 1.1, true);
	wp_enqueue_script( 'imagezoom', get_template_directory_uri() . '/assets/js/imagezoom.js', array ( 'jquery' ), 1.1, true);
	wp_enqueue_script( 'chocolat', get_template_directory_uri() . '/assets/js/jquery.chocolat.js', array ( 'jquery' ), 1.1, true);
	wp_enqueue_script( 'flexslider', get_template_directory_uri() . '/assets/js/jquery.flexslider.js', array ( 'jquery' ), 1.1, true);
	wp_enqueue_script( 'magnific', get_template_directory_uri() . '/assets/js/jquery.magnific-popup.js', array ( 'jquery' ), 1.1, true);
	wp_enqueue_script( 'jstarbox', get_template_directory_uri() . '/assets/js/jstarbox.js', array ( 'jquery' ), 1.1, true);
	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/assets/js/modernizr.custom.72111.js', array (), 1.1, true);
	wp_enqueue_script( 'simpleCart', get_template_directory_uri() . '/assets/js/simpleCart.min.js', array ( 'jquery' ), 1.1, true);
	wp_enqueue_script( 'uisearch', get_template_directory_uri() . '/assets/js/uisearch.js', array (), 1.1, true);
	wp_enqueue_script( 'script', get_template_directory_uri() . '/assets/js/script.js' , array ( 'jquery' ), 1.1, true);

}
add_action('wp_enqueue_scripts', 'theme_scripts');

/**
 * Wordpress Bootstrap DropDown Navigation
 */
require get_template_directory() . '/inc/wp_bootstrap_navwalker.php';

/**
 * Create option page
 */
if ( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 		=> 'Main info',
		'icon_url'			=> 'dashicons-format-aside',
		'position'			=> 3
	));
}

/**
 * Enable WooCommerce to use this theme
 */
add_theme_support("woocommerce");


/**
 * Move the breadcrumbs outside the wrapper
 */

// Reposition WooCommerce breadcrumb
function woocommerce_remove_breadcrumbs() {
	remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
}
add_action('woocommerce_before_main_content', 'woocommerce_remove_breadcrumbs');

// Shopin Bread Crumbs
function shopin_breadcrumbs() {
	woocommerce_breadcrumb();
}
add_action('shopin_breadcrumbs', 'shopin_breadcrumbs');

/**
 * Products Custom thumbs
 */
remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);


function shopin_product_thumb() {
	global $post;
	$image_link = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ));

	echo "<div class='pro-img'>";
	echo "<img src='{$image_link}' class='img-responsive'>";
	echo "<div class='zoom-icon '>";
	echo "<a class=\"picture\" href='{$image_link}' rel=\"title\" class=\"b-link-stripe b-animate-go  thickbox\"><i class=\"glyphicon glyphicon-search icon \"></i></a>";
	echo "<a href='" . get_permalink( $post->ID ) ."'><i class=\"glyphicon glyphicon-menu-right icon\"></i></a>";
	echo "</div>";
	echo "</div>";

}
add_action('shopin_product_thumb', 'shopin_product_thumb');


/**
 * Products custom title title
 */
remove_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title');

function shopin_product_title() {
	global $post;

	$terms = get_the_terms( $post->ID, 'product_cat' );

	echo '<span>';
	foreach ($terms as $term) {
		if ($term->parent == true) {
			echo $term->name;
		}
	}
	echo '</span>';
	echo "<h6 class='clearfix'>";
	echo "<a href='". get_permalink($post->ID) ."'>";
	echo get_the_title();
	echo "</a></</h6>";
}
add_action('shopin_product_title', 'shopin_product_title');

/**
 * Custom Ratting System
 */

remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);

function shopin_product_ratting() {
	global $product;
	$average_rate = $product->get_average_rating() / 5;

	echo "<div class='block'>";
	echo "<div class='starbox small ghosting' data-start-value='{$average_rate}'></div>";
	echo "</div>";
	
}
add_action('shopin_product_ratting', 'shopin_product_ratting');

/**
 * Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php)
 */
add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );
function woocommerce_header_add_to_cart_fragment( $fragments ) {
	ob_start();
	?>
	<a class="cart-contents" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php echo WC()->cart->get_cart_total(); ?></a>
	<?php

	$fragments['a.cart-contents'] = ob_get_clean();

	return $fragments;
}

/**
 * Disable the default stylesheet WooCommerce
 */
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

/**
 * Redesign rating and price
 */

// Rating
remove_action('woocommerce_single_product_summary','woocommerce_template_single_rating');
add_action('shopin_woocommerce_template_single_rating', 'woocommerce_template_single_rating');

// Price	
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price');
add_action('shopin_woocommerce_template_single_price', 'woocommerce_template_single_price');
	
// title
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
add_action('shopin_woocommerce_template_single_title', 'woocommerce_template_single_title');
	
// Products meta
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);

// Info tsb
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs');
add_action('shopin_woocommerce_output_product_data_tabs', 'woocommerce_output_product_data_tabs');


/**
 * CheckOut page Bootstrap fields class
 */
add_filter('woocommerce_checkout_fields', 'addBootstrapToCheckoutFields' );
function addBootstrapToCheckoutFields($fields) {
	foreach ($fields as &$fieldset) {
		foreach ($fieldset as &$field) {
			// if you want to add the form-group class around the label and the input
			$field['class'][] = 'form-group';

			// add form-control to the actual input
			$field['input_class'][] = 'form-control';
		}
	}
	return $fields;
}

// Removes Product Successfully Added to Cart
// WooCommerce 2.1+

add_filter( 'wc_add_to_cart_message', 'bbloomer_custom_add_to_cart_message' );

function bbloomer_custom_add_to_cart_message() {

	echo '<style>.woocommerce-message {display: none !important;}</style>';

}