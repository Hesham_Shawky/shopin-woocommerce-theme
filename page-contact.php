<?php
/**
 * Template Name: Contact Page
 */
// Header
get_header();

// Banner
get_template_part('template-parts/content', 'banner_page');

?>

    <div class="contact">

        <div class="contact-form">
            <div class="container">
                <div class="col-md-6 contact-left">
                    <h3><?php the_field('description_title'); ?></h3>
                    <p><?php the_field('about_shopin'); ?></p>


                    <div class="address">
                        <div class=" address-grid">
                            <i class="glyphicon glyphicon-map-marker"></i>
                            <div class="address1">
                                <h3>Address</h3>
                                <p><?php the_field('shopin_adress'); ?></p>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                        <div class=" address-grid ">
                            <i class="glyphicon glyphicon-phone"></i>
                            <div class="address1">
                                <h3>Our Phone:<h3>
                                        <p><?php the_field('shopin_phone'); ?></p>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                        <div class=" address-grid ">
                            <i class="glyphicon glyphicon-envelope"></i>
                            <div class="address1">
                                <h3>Email:</h3>
                                <p><a href="mailto:<?php the_field('shopin_email'); ?>"> <?php the_field('shopin_email'); ?></a></p>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                        <div class=" address-grid ">
                            <i class="glyphicon glyphicon-bell"></i>
                            <div class="address1">
                                <h3>Open Hours:</h3>
                                <p><?php the_field('shopin_open_hours'); ?></p>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 contact-top">
                    <h3>Want to work with me?</h3>
                   <?php
                   if (have_posts()) : the_post(); the_content(); endif;
                   ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="map">
            <?php
            $location = get_field('shopin_location');
            if( !empty($location) ):?>
                <div class="acf-map">
                    <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
                </div>

            <?php endif; ?>
        </div>
    </div>

<?php
// Footer
get_footer();