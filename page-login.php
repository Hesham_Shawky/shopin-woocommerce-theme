<?php
/**
 * Template Name: Login Page
 */


if (is_user_logged_in())
    header("Location:". site_url('my-account') ."");

get_header();




get_template_part('template-parts/content', 'banner_page');

echo '<div class="product">
        <div class="container">
            <div class="row">
                <div class="col-sm-6"> ';
get_template_part('woocommerce/myaccount/form', 'login');

echo '</div><!-- //.col -->
        <div class="col-sm-6 login-right">
            <h3>Completely Free Account</h3><p>';

if (have_posts()): the_post(); the_content(); endif;

echo '</p>
        <a href="'. site_url("register") .'" class=" hvr-skew-backward">Register</a></div><!-- //.col --></div><!-- //.row --></div><!-- //.container --></div><!-- //.product -->';


get_footer();