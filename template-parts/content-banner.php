<!--banner-->
<div class="banner">
    <div class="container">
        <section class="rw-wrapper">
            <h1 class="rw-sentence">
                <span>Fashion &amp; Beauty</span>
                <div class="rw-words rw-words-1">
                    <?php
                    while (have_rows('banner', 'option')) {
                        the_row();
                        echo '<span>';
                        the_sub_field('first_word');
                        echo '</span>';
                    }
                    ?>
                </div>
                <div class="rw-words rw-words-2">
                    <?php
                    while (have_rows('banner', 'option')) {
                        the_row();
                        echo '<span>';
                        the_sub_field('second_word');
                        echo '</span>';
                    }
                    ?>
                </div>
            </h1>
        </section>
    </div>
</div>