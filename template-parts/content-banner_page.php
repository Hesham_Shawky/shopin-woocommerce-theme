<div class="banner-top">
    <div class="container">
        <h1>
            <?php
            if ( is_404() ) 
                echo '404';

            elseif (is_shop())
                woocommerce_page_title();

            elseif (is_product_category())
                woocommerce_page_title();

            elseif(apply_filters( 'woocommerce_show_page_title', true ))
                the_title();


            ?>
            
        </h1>
        <em></em>
        <h2><?php do_action('shopin_breadcrumbs'); ?></h2>
    </div>
</div>