<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Shopin
 */

// Site Header
get_header();

// Banner Pages
get_template_part('template-parts/content', 'banner_page');

?>
	<div class="product">
		<div class="container">

			<div class="four">
				<h3>404</h3>
				<p class="page-title">
					<?php esc_html_e( 'Sorry! Evidently the document you were looking for has either been moved or no longer exist.', 'shopin' ); ?>
				</p>
				<a href="<?php echo site_url("/"); ?>" class="hvr-skew-backward">Back To Home</a>
			</div>


		</div>
	</div>

<?php
get_footer();
