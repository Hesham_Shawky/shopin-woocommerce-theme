<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Shopin
 */

// Site Header
get_header();

// Banner Pages
get_template_part('template-parts/content', 'banner_page');

?>

	<div class="product">
		<div class="container">
			<div class="row">
				<main class="<?php if ( is_checkout() || is_cart() )  echo 'col-md-12'; else echo 'col-md-9'; ?>" role="main">
					<?php
					while ( have_posts() ) : the_post();

						the_content();

					endwhile; // End of the loop.
					?>
				</main>

				<?php if ( !is_cart() && !is_checkout() ) : ?>
					<aside class="col-md-3">
						<?php get_sidebar(); ?>
					</aside>
				<?php endif; ?>
			</div>

		</div>
	</div>


<?php
get_footer();
