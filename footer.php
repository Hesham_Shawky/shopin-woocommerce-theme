<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Shopin
 */

?>
<!--brand-->
<div class="container">
	<div class="brand">
		<?php
			while (have_rows('brands', 'option')): the_row();
		?>
		<div class="col-md-3 brand-grid">
			<img src="<?php the_sub_field('brand_logo')[URL]; ?>" class="img-responsive">
		</div>
		<?php endwhile; ?>
		<div class="clearfix"></div>
	</div>
</div>
<!--//brand-->
<!--//footer-->
<div class="footer">
	<div class="footer-middle">
		<div class="container">
			<?php
				dynamic_sidebar('footer');
			?>
			<div class="clearfix"> </div>
		</div>
	</div>
	<div class="footer-bottom">
		<div class="container">
			<ul class="footer-bottom-top">
				<li><a href="#"><img src="<?php bloginfo('stylesheet_directory');  ?>/assets/images/f1.png" class="img-responsive" alt=""></a></li>
				<li><a href="#"><img src="<?php bloginfo('stylesheet_directory');  ?>/assets/images/f2.png" class="img-responsive" alt=""></a></li>
				<li><a href="#"><img src="<?php bloginfo('stylesheet_directory');  ?>/assets/images/f3.png" class="img-responsive" alt=""></a></li>
			</ul>
			<p class="footer-class">&copy; 2016 Shopin. All Rights Reserved | Design by  <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<!--//footer-->	
</div><!-- #page -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEp3QdrVDfspBSAvoTgYtWHjuIMI6PzF8"></script>
<?php wp_footer(); ?>

</body>
</html>
