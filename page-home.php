<?php
/**
 * User: Hisham Shawky
 * Template Name: Home Page
 */

// Get the header
get_header();

// Home Banner

get_template_part('template-parts/content', 'banner');

echo "<br><div class='content-mid'><h3>Trending Items</h3><label class=\"line\"></label></div> ";
if (have_posts()): the_post();
    echo "<div class='product'>";
        the_content();
    echo "</div>";
endif;

// Get the footer
get_footer();