<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Shopin
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body >
<div id="page" class="site">
	<!--header-->
	<div class="header">
		<div class="container">
			<div class="head">
				<div class=" logo">
					<a href="<?php site_url('/'); ?>"><img src="<?php the_field('logo', 'option')['url']; ?>" alt="Shopin" title="Shopin"></a>
				</div>
			</div>
		</div>
		<div class="header-top">
			<div class="container">
				<?php
					wp_nav_menu(array(
						'menu'	=> is_user_logged_in() ? 'logged in menu' : 'logged out menu',
						'container_class'	=> 'col-sm-5 col-md-offset-2  header-login'
					));
				?>


				<div class="col-sm-5 header-social">
					<ul >
						<li><a href="<?php the_field('twitter', 'option')?>"><i></i></a></li>
						<li><a href="<?php the_field('facebook', 'option')?>"><i class="ic1"></i></a></li>
						<li><a href="<?php the_field('linkedin', 'option')?>"><i class="ic3"></i></a></li>
						<li><a href="<?php the_field('rss', 'option')?>"><i class="ic4"></i></a></li>
					</ul>

				</div>
				<div class="clearfix"> </div>
			</div>
		</div>

		<div class="container">

			<div class="head-top">

				<div class="col-sm-8 col-md-offset-2 h_menu4">
					<nav class="navbar nav_bottom" role="navigation">

						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header nav_2">
							<button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>

						</div>

						<!-- Collect the nav links, forms, and other content for toggling -->
						<?php
							wp_nav_menu(array(
								'theme_location' 	=> 'primary',
								'container'			=> 'div',
								'container_class'	=> 'collapse navbar-collapse',
								'container_id'		=> 'bs-megadropdown-tabs',
								'menu_class'		=> 'nav navbar-nav nav_1',
								'fallback_cb'		=> 'wp_bootstrap_navwalker::fallback',
								'walker'			=> new wp_bootstrap_navwalker()
							));
						?>
						


					</nav>
				</div>
				<div class="col-sm-2 search-right">
					<ul class="heart">
						<li>
							<a href="<?php echo site_url('wishlist'); ?>" >
								<span class="glyphicon glyphicon-heart" aria-hidden="true"></span>
							</a></li>
						<li><a class="play-icon popup-with-zoom-anim" href="#small-dialog"><i class="glyphicon glyphicon-search"> </i></a></li>
					</ul>
					<div class="cart box_1">
						<a href="<?php echo WC()->cart->get_cart_url(); ?>">
							<h3> <div class="total">
									<a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php echo WC()->cart->get_cart_total(); ?></a>
									<?php
									woocommerce_header_add_to_cart_fragment($fragments)
									?>
									</div>
								<img src="<?php bloginfo('stylesheet_directory') ?>/assets/images/cart.png" alt=""/></h3>
						</a>
						<p><a href="javascript:;" class="simpleCart_empty">Empty Cart</a></p>

					</div>
					<div class="clearfix"> </div>

					<!----->


					<!---//pop-up-box---->
					<div id="small-dialog" class="mfp-hide">
						<div class="search-top">
							
							<?php get_product_search_form() ?>
							<p>Shopin</p>
						</div>
					</div>
					<!----->
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>