<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>



        <form method="post" class="register">

            <?php do_action( 'woocommerce_register_form_start' ); ?>

            <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide login-mail">
                <input placeholder="Youe Email" type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email" value="<?php if ( ! empty( $_POST['email'] ) ) echo esc_attr( $_POST['email'] ); ?>" required/>
                <i  class="glyphicon glyphicon-user"></i>
            </p>

            <?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

                <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide login-mail">
                    <input placeholder="Type your password" type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password" id="reg_password" required/>
                    <i class="glyphicon glyphicon-lock"></i>
                </p>

            <?php endif; ?>

            <!-- Spam Trap -->
            <div style="<?php echo ( ( is_rtl() ) ? 'right' : 'left' ); ?>: -999em; position: absolute;"><label for="trap"><?php _e( 'Anti-spam', 'woocommerce' ); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" autocomplete="off" /></div>

            <?php do_action( 'woocommerce_register_form' ); ?>
            <?php do_action( 'register_form' ); ?>

            <p class="woocomerce-FormRow form-row">
                <?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
                <input type="submit" class="woocommerce-Button item_add hvr-skew-backward " name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>" />
            </p>

            <?php do_action( 'woocommerce_register_form_end' ); ?>

        </form>
    
<?php

endif;

?>