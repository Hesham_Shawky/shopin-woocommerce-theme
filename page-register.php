<?php
/**
 * Template Name: Register Page
 */
if ( is_user_logged_in() )
    header("location:".site_url('my-account')."");
// Header
get_header();

// Banner
get_template_part('template-parts/content', 'banner_page');

echo "<div class='login'>
        <div class='container'>
            <div class='row'> ";

echo "<div class='col-sm-6 login-do'>";

get_template_part('woocommerce/myaccount/form', 'register');

echo "</div><!-- //.col -->";

echo "<div class='col-sm-6 login-right'>
        <h3>Completely Free Account</h3>
        <p>";

if (have_posts()) : the_post(); the_content(); endif;

echo "</p>
<a href='". site_url('login') ."' class=\"hvr-skew-backward\">Login</a>
</div><!-- //.col -->";

echo "</div><!-- //.row -->
    </div><!-- //.container -->
</div><!-- //.product -->";


// Footer
get_footer();